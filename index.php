<?php

    require_once 'vendor/autoload.php';

    use Lab2\Lab2\Deliver;
    use Lab2\Lab2\Transport;
    use Lab2\Lab2\Box;
    use Lab2\Lab2\Truck;
    use Lab2\Lab2\Train;
    use Lab2\Lab2\Plane;


    $giftSasha = new Box("Подарок Саше", 3);
    $sofa = new Box("Диван", 150);
    $notebook = new Box("Подарок Оле", 2);

    $train = new Train();
    $train->deliver($giftSasha, "Архангельск");
    $train->deliver($sofa, "Урюпинск");
    $train->deliver($notebook, "Магадан");

    $plane = new Plane(20);
    $plane->deliver($giftSasha, "Урюпинск");
    $plane->deliver($sofa, "Москва");
    $plane->deliver($notebook, "Магадан");

    $truck = new Truck();
    $truck->deliver($giftSasha, "Урюпинск");
    $truck->deliver($sofa, "Москва");
    $truck->deliver($notebook, "Магадан");

?>