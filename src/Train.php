<?php

    namespace Lab2\Lab2;

    class Train extends Transport implements Deliver {
        const locations = [
            "Москва" => 500,
            "Архангельск" => 700,
            "Урюпинск" => 500
        ];

        public function canDeliver (int $weight, string $city) {
            if(!array_key_exists($city, $this::locations)) {
                echo "<pre>Доставка поездом в город {$city} не осуществляется</pre>";
                return false;
            }

            return true;
        }
    }

?>