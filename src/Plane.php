<?php

    namespace Lab2\Lab2;

    class Plane extends Transport implements Deliver {
        public $capacity;
        const locations = [
            "Москва" => 800,
            "Архангельск" => 1200,
            "Магадан" => 1500
        ];

        public function __construct(int $capacity) {
            $this->time = $time;
            $this->capacity = $capacity;
            $this->price = $price;
        }

        public function canDeliver (int $weight, string $city) {
            if($weight > $this->capacity) {
                echo "<pre>Доставка самолетом невозможна: превышен допустимый вес {$this->capacity} кг</pre>";
                return false;
            }

            if(!array_key_exists($city, $this::locations)) {
                echo "<pre>Доставка самолетом в город {$city} не осуществляется</pre>";
                return false;
            }

            return true;
        }
    }

?>