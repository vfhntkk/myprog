<?php

    namespace Lab2\Lab2;

    class Truck extends Transport implements Deliver {
        const locations = [
            "Москва" => 600,
            "Архангельск" => 800,
            "Урюпинск" => 700
        ];

        public function canDeliver (int $weight, string $city) {
            if(!array_key_exists($city, $this::locations)) {
                echo "<pre>Доставка грузовиком в город {$city} не осуществляется</pre>";
                return false;
            }

            return true;
        }
    }

?>