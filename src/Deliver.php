<?php

    namespace Lab2\Lab2;

    interface Deliver {
        public function canDeliver(int $weight, string $city);
    }

?>