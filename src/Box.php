<?php

    namespace Lab2\Lab2;

    class Box {
        public $name;
        public $weight;

        public function __construct(string $name, int $weight) {
            $this->name = $name;
            $this->weight = $weight;
        }
    }

?>