<?php

    namespace Lab2\Lab2;

    class Transport {
        public function deliver(Box $box, string $location) {
            if($this->canDeliver($box->weight, $location)) {
                echo "<pre>Ваша посылка '{$box->name}' будет доставлена в город {$location}. Стоимость доставки {$this->getCost($box->weight, $location)} руб.</pre>";
            }
        }

        public function getCost(int $weight, string $location) {
            return $weight * $this::locations[$location];
        }
    }

?>